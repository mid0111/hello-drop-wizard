package com.example;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.lang.Exception;

/**
 * Application メインクラス.
 * Created by mid on 14/06/11.
 */
public class HelloDropwizard extends Application<HelloDropwizardConfiguration> {

    @Override
    public void initialize(Bootstrap<HelloDropwizardConfiguration> bootstrap) {

    }

    @Override
    public void run(HelloDropwizardConfiguration configuration, Environment environment) {
        final HelloWorldResource resource = new HelloWorldResource(
                configuration.getTemplate(),
                configuration.getDefaultName()
        );
        environment.jersey().register(resource);
    }

    /**
     * main.
     * @param args args
     */
    public static void main(String[] args) throws Exception {
        new HelloDropwizard().run(args);
    }

}
