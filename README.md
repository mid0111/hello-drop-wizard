hello-drop-wizard
================

[Dropwizard](git remote add origin git@bitbucket.org:mid0111/hello-drop-wizard.git)で遊んでみる用。

## Usage

### Create jar file

```bash
% ./gradlew oneJar
```

and check the created jar file is corrected.

```bash
% java -jar build/libs/hello-drop-wizard-0.0.1-standalone.jar 

usage: java -jar project.jar [-h] [-v] {server,check} ...

positional arguments:
  {server,check}         available commands

optional arguments:
  -h, --help             show this help message and exit
  -v, --version          show the application version and exit

```

### run application server

```bash
% java -jar build/libs/hello-drop-wizard-0.0.1-standalone.jar server src/main/resources/HelloDropwizard.yml 

% curl http://localhost:8080/ -i ; echo
HTTP/1.1 200 OK
Date: Wed, 11 Jun 2014 13:34:41 GMT
Content-Type: application/json
Vary: Accept-Encoding
Transfer-Encoding: chunked

{"id":6,"content":"Hello, Stranger!"}
```
